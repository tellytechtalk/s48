
//This is a mock database
let posts = [];

//Represents the ID since JS lang ito at hindi mongoDB na naggegenerate ng id automatically
let count = 1;


//This function will show/display the posts 
const showPosts = (posts) => {
	
	//Girl, dito papasok lahat ng entries 
	let post_entries = ''

//This part loops or dindaanan nya lahat ng entries na to at inaassign nya isa-isa sa matching html 
	posts.forEach((post)=> {

// ito ang representation ng bawat isang post entry:
		post_entries += 	`
			<div id="post-${post.id}">
			    <h3 id="post-title-${post.id}">${post.title}</h3>
			    <p id="post-body-${post.id}">${post.body}</p>
			    <button onclick="editPost('${post.id}')">Edit</button>
			    <button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});

//then the post_entries will be assigned in "div-post-entries"
	document.querySelector('#div-post-entries').innerHTML = post_entries
}


// [SECTION] Adding a new post
document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	event.preventDefault()

	//addEventListener: kapag may nagpindot na sa submit button sa add-post na part ng form gagana na siya at mapupush na yung info from id, input na Title and body na Text area sa post array

	posts.push({
		id: count, 
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});

	// For incrementing the ID to be unique for each new post 
	count++


	// Call the posts in order to show the updated/added posts 
	showPosts(posts);
	alert('Successfully added a new Post!');


/*	NOTES: 
		1. hindi magse-stay ang posts dahil array lang sya walang database na nakakabit
		2. Bakit nagrerefresh ang page? 
		Dahil ang default behavior ng form kapag nagsubmit ka irerefresh nya ang buong page. 
		3. "onclick": built in for button clicking 
*/

})


//[SECTION] Editing a post
const editPost = (id) => {

	//This will set the body and title input from the edit form
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
}


// [SECTION] Updating a post
// (e) = can be used as an event shortcut

document.querySelector('#form-edit-post').addEventListener('submit', (event) => {
    event.preventDefault();

	//loop through the array to find the match
    for (let i = 0; i < posts.length; i++) {
        // The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
        // Therefore, it is necesary to convert the Number to a String first.

        //If match has been found , then proceed with assigning the new values to the existing post (The if statement below sets the existing posts input )

        if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;

            // Display the updated lists of posts once this specific post has been updated 
    
            showPosts(posts);
            alert('Post Successfully updated.');
            
            break;
        }
    }
});

// [SECTION] Deleting a post 
const deletePost = (id) => {
	
	//ACTIVITY
	/*
		-Build deletePost function to be able to delete a specific post from the array
		- SHow the updated list of posts after deleting 

		TIPS: 
		1. You may use a loop to check for the matching post, 
		2. You may also use the .filter()array method and remove the specific post returned from that method

	*/


	let index = parseInt(`${id}`); 
	
		for(let i = 0; i < posts.length; i++) {
		   
		    if(posts[i].id === index) {
		        posts.splice(i, 1);


		        showPosts(posts);
		        alert('Post successfully deleted');

		        break;
		    }
		}
};

